#include <pebble.h>

#define CBC 1
#include "aes.h"

static Window *s_window;
static TextLayer *s_text_layer;

// 128bit key
// TODO: make new key and iv
uint8_t KEY[16] = { (uint8_t) 0x2b, (uint8_t) 0x7e, (uint8_t) 0x15, (uint8_t) 0x16, (uint8_t) 0x28, (uint8_t) 0xae, (uint8_t) 0xd2, (uint8_t) 0xa6, (uint8_t) 0xab, (uint8_t) 0xf7, (uint8_t) 0x15, (uint8_t) 0x88, (uint8_t) 0x09, (uint8_t) 0xcf, (uint8_t) 0x4f, (uint8_t) 0x3c };
uint8_t IV[]  = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f }; 

// Largest expected inbox and outbox message sizes
const uint32_t INBOX_SIZE = 1024;
const uint32_t OUTBOX_SIZE = 512;

static int hex_to_int(char hex_byte[], int len) {
  int base = 1;
  int int_val = 0;

  for (int i = len - 1; i >= 0; i--) {
    if (hex_byte[i]>='0' && hex_byte[i]<='9') { 
      int_val += (hex_byte[i] - 48)*base; 

      base = base * 16; 
    } else if (hex_byte[i]>='a' && hex_byte[i]<='f') { 
      int_val += (hex_byte[i] - 87)*base; 

      base = base*16; 
    }
  }

  return int_val;
}

static void decrypt(char buf[]) {
  struct AES_ctx ctx;

  int hex_string_size = strlen(buf);

  // create buffer to decode hex in
  char decode_buf[hex_string_size];
  memcpy(decode_buf, buf, hex_string_size);

  // for every 1 hex byte ie 2 chars there is 1 int
  uint8_t cipher_buf[hex_string_size/2];
  memset(cipher_buf, 0, sizeof(cipher_buf));
  int i, j;
  for (i = 0, j = 0; i < hex_string_size; i += 2, j++) {
    char hex_byte[2];
    int hex_byte_size = sizeof(char) * 2;

    // read 2 char bytes of the hex string into buffer
    memcpy(&hex_byte, decode_buf + i, hex_byte_size);

    int int_val = hex_to_int(hex_byte, hex_byte_size);
    
    cipher_buf[j] = int_val;
  }

  int cipher_buf_size = sizeof(cipher_buf);

  // decrypt challenge
  AES_init_ctx_iv(&ctx, KEY, IV);
  AES_CBC_decrypt_buffer(&ctx, cipher_buf, cipher_buf_size);

  // copy decrypted string back into buffer passed to function
  memset(buf, 0, hex_string_size);
  memcpy(buf, cipher_buf, cipher_buf_size);

  int pad = cipher_buf[cipher_buf_size - 1];
  for (int i = cipher_buf_size - 1; i >= 0; i--) {
    if (buf[i] != pad) {
      buf[i + 1] = '\0';
      break;
    }
  }
}

static int encrypt(char buf[]) {
  int blocksize = 16;

  int plaintext_len = strlen(buf);
  int pad_size = blocksize - (plaintext_len % blocksize);
  int cipher_size = plaintext_len + pad_size;

  uint8_t cipher_buf[cipher_size];
  memcpy(cipher_buf, buf, cipher_size);
  memset(cipher_buf + plaintext_len, (char)pad_size, pad_size);

  struct AES_ctx ctx;
  AES_init_ctx_iv(&ctx, KEY, IV);

  AES_CBC_encrypt_buffer(&ctx, cipher_buf, cipher_size);

  char hex_buf[cipher_size * 2];
  int j = 0;
  for (int i = 0; i < cipher_size; i++) {
    j += snprintf(hex_buf + j, 4, "%02x", cipher_buf[i]);
  }

  memcpy(buf, hex_buf, sizeof(hex_buf));
  return sizeof(hex_buf);
}

static void request_challenge(){
  DictionaryIterator *out_iter;

  char message[] = "requesting challenge";

  AppMessageResult result = app_message_outbox_begin(&out_iter);
  if(result == APP_MSG_OK) {
    dict_write_cstring(out_iter, MESSAGE_KEY_challenge, message);

    // Send this message
    result = app_message_outbox_send();
    if(result != APP_MSG_OK) {
      APP_LOG(APP_LOG_LEVEL_ERROR, "Error sending the outbox: %d", (int)result);
    }
  } else {
    // The outbox cannot be used right now
    APP_LOG(APP_LOG_LEVEL_ERROR, "Error preparing the outbox: %d", (int)result);
  }
}

static void send_response(char message[]) {
  DictionaryIterator *out_iter;

  int cipher_size = encrypt(message);
  // explicitly set null bite
  message[cipher_size] = '\0';

  AppMessageResult result = app_message_outbox_begin(&out_iter);
  if(result == APP_MSG_OK) {
    dict_write_cstring(out_iter, MESSAGE_KEY_response, message);

    // Send this message
    result = app_message_outbox_send();
    if(result != APP_MSG_OK) {
      APP_LOG(APP_LOG_LEVEL_ERROR, "Error sending the outbox: %d", (int)result);
    }
  } else {
    // The outbox cannot be used right now
    APP_LOG(APP_LOG_LEVEL_ERROR, "Error preparing the outbox: %d", (int)result);
  }
}

static void append_an_a(char buf[]) {
  int str_len = strlen(buf);
  buf[str_len] = 'a';
  buf[str_len + 1] = '\0';
}

static void prv_select_click_handler(ClickRecognizerRef recognizer, void *context) {
  text_layer_set_text(s_text_layer, "Requesting Challenge");
  request_challenge();
}

static void prv_up_click_handler(ClickRecognizerRef recognizer, void *context) {
  text_layer_set_text(s_text_layer, "Up");
}

static void prv_down_click_handler(ClickRecognizerRef recognizer, void *context) {
  text_layer_set_text(s_text_layer, "Down");
}

static void prv_click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, prv_select_click_handler);
  window_single_click_subscribe(BUTTON_ID_UP, prv_up_click_handler);
  window_single_click_subscribe(BUTTON_ID_DOWN, prv_down_click_handler);
}

static void prv_window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  s_text_layer = text_layer_create(GRect(0, 72, bounds.size.w, 20));
  text_layer_set_text(s_text_layer, "Press a button");
  text_layer_set_text_alignment(s_text_layer, GTextAlignmentCenter);
  layer_add_child(window_layer, text_layer_get_layer(s_text_layer));
}

static void inbox_received_callback(DictionaryIterator *iter, void *context) {
  Tuple *ready_tuple = dict_find(iter, MESSAGE_KEY_challenge);
  char *challenge = ready_tuple->value->cstring;

  decrypt(challenge);

  char buf[INBOX_SIZE];
  memcpy(buf, challenge, strlen(challenge) + 1);
  append_an_a(buf);
  send_response(buf);
}

static void message_init() {
  // Open AppMessage
  app_message_open(INBOX_SIZE, OUTBOX_SIZE);
  app_message_register_inbox_received(inbox_received_callback);
}

static void prv_window_unload(Window *window) {
  text_layer_destroy(s_text_layer);
}

static void prv_init(void) {
  s_window = window_create();
  window_set_click_config_provider(s_window, prv_click_config_provider);
  window_set_window_handlers(s_window, (WindowHandlers) {
    .load = prv_window_load,
    .unload = prv_window_unload,
  });
  const bool animated = true;
  window_stack_push(s_window, animated);
  
  message_init();
}

static void prv_deinit(void) {
  window_destroy(s_window);
}

int main(void) {
  prv_init();

  /* APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", s_window); */

  app_event_loop();
  prv_deinit();
}
