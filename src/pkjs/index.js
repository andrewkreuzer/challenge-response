Pebble.addEventListener('ready', function() {
  init.eventListeners();
});


init = {
  eventListeners: function () {
    Pebble.addEventListener('appmessage', function(e) {
      appmessage(e);
    });
  }
}

function appmessage(e) {
  var dict = e.payload;

  if (dict['challenge']) {
    console.log("requesting challenge");
    request_challenge();
    return;
  }
  if (dict['response']) {
    send_response(JSON.stringify(dict['response']));
  }
}

function request_challenge() {
  var request = new XMLHttpRequest();

  request.onload = function() {
    var dict = {
      'challenge': this.responseText,
    }
    Pebble.sendAppMessage(dict, function() {
      console.log("Sent challenge to watch for decryption");
    }, function(e) {
      console.log('Unable to send challenge to watch: ' + JSON.stringify(e));
    });
  }

  request.open('GET', 'http://192.168.1.100:8080/challenge');
  request.send()
}

function send_response(response) {
  var request = new XMLHttpRequest();

  console.log(response);
  request.onload = function() {
    console.log("Server response: " + this.responseText);
  }
  request.open('POST', 'http://192.168.1.100:8080/response');
  request.send(response);
}
