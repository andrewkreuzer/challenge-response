use std::str;
use std::error;
use std::sync::Arc;
use std::iter;
use rand::{Rng, thread_rng};
use rand::distributions::Alphanumeric;

use hyper::body;
use hyper::{Body, Request, Response, Server, Method};
use hyper::service::{make_service_fn, service_fn};

use tokio::sync::Mutex;

use serde_json::Value;

use aes::Aes128;
use block_modes::{BlockMode, Cbc};
use block_modes::block_padding::Pkcs7;
use hex_literal::hex;

async fn create_challenge() -> Result<String, Box<dyn error::Error>> {
    let mut rng = thread_rng();

    let challenge_size = rng.gen_range(32, 62);
    println!("Challenge size: {}", challenge_size);

    let challenge_string: String = iter::repeat(())
        .map(|()| rng.sample(Alphanumeric))
        .take(challenge_size)
        .collect();

    Ok(challenge_string)
}

fn encrypt_str(challenge_string: &str) -> String {
    type Aes128Cbc = Cbc<Aes128, Pkcs7>;

    let iv = hex!("000102030405060708090a0b0c0d0e0f");
    let key = hex!("2b7e151628aed2a6abf7158809cf4f3c");
    // let plaintext = b"Lorem ipsum dolor sit amet, consectetur adipiscing elit posuere.";
    let plaintext = challenge_string.as_bytes();
    let cipher = Aes128Cbc::new_var(&key, &iv).unwrap();

    // buffer must have enough space for message+padding
    let mut buffer = [0u8; 1024];
    // copy message to the buffer
    let pos = plaintext.len();
    buffer[..pos].copy_from_slice(plaintext);

    let ciphertext = cipher.encrypt(&mut buffer, pos).unwrap();

    let mut hex_buf = String::new();
    for n in ciphertext {
        hex_buf.push_str(format!("{:02x}", n).as_ref());
    }

    // println!("{}", hex_buf);
    hex_buf.into()
}

fn decrypt_str(enc_response: String) -> String {
    type Aes128Cbc = Cbc<Aes128, Pkcs7>;

    let iv = hex!("000102030405060708090a0b0c0d0e0f");
    let key = hex!("2b7e151628aed2a6abf7158809cf4f3c");
    let cipher = Aes128Cbc::new_var(&key, &iv).unwrap();
    let mut decoded_hex = hex::decode(enc_response).unwrap();
    let decrypted_ciphertext = cipher.decrypt(&mut decoded_hex).unwrap();

    let decrypted_str = str::from_utf8(decrypted_ciphertext).unwrap();
    // println!("{}", decrypted_str);

    decrypted_str.into()

}

async fn service_handler(

            req: Request<Body>, 
            challenge: Arc<Mutex<Option<String>>>

    ) -> Result<Response<Body>, hyper::Error> {

    let mut challenge = challenge.lock().await;

    match (req.method(), req.uri().path()) {
        (&Method::POST, "/") => {
            let enc = encrypt_str("Lorem ipsum dolor sit amet, consectetur adipiscing elit posuere.");

            let bytes = body::to_bytes(req.into_body()).await?;
            let json: Value = serde_json::from_str(str::from_utf8(&bytes)
                    .expect("req body bytes to string conversion failed"))
                .expect("parsing of the request body failed");

            println!("{}", enc == json["enc"]);

            Ok(Response::new("Hello, World".into()))
        },
        (&Method::GET, "/challenge") => {
            // *challenge = Some("a random challenge can you solve it.".to_string());
            *challenge = Some(create_challenge()
                             .await
                             .unwrap_or("unable to aquire challenge".to_string()));

            let challenge = match &mut *challenge {
                Some(s) => s.clone(),
                None => panic!("no challenge"),

            };

            let enc_string = encrypt_str(&challenge);

            Ok(Response::new(enc_string.into()))
        },
        (&Method::POST, "/response") => {
            let bytes = body::to_bytes(req.into_body()).await?;
            // let json: Value = serde_json::from_str(str::from_utf8(&bytes)
            //         .expect("req body bytes to string conversion failed"))
            //     .expect("parsing of the request body failed");
            let hex = str::from_utf8(&bytes)
                .expect("hex to string failure")
                .replace("\"", "");

            // println!("{}", hex);
            // TODO: I'm starting to get doubtful of using .into() all the time 
            //it gives no indication as to what From trait is being used
            let response_plaintext = decrypt_str(hex.into());




            if let Some(c) = &mut *challenge {
                c.push_str("a");
                println!("\nChallenge: {}\n", c);
                println!("Response: {}\n", response_plaintext);
                if c == &response_plaintext {
                    Ok(Response::new("ya done did it".into()))
                } else {
                    Ok(Response::new("Not a chance fool".into()))
                }
            } else {
                Ok(Response::new("You have not requested a challenge".into()))
            }

        }
        _ => {
            println!("sending default response");
            Ok(Response::new("Hello, World".into()))
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let addr = ([0, 0, 0, 0], 8080).into();

    let challenge = Arc::new(Mutex::new(None::<String>));

    let service = make_service_fn(move |_| { 

        let challenge_ref = Arc::clone(&challenge);

        async move {
            Ok::<_, hyper::Error>(service_fn(move |req| {
                service_handler(req, challenge_ref.clone())
            }))
        }

    });

    let server = Server::bind(&addr).serve(service);

    println!("Listening on http://{}", addr);

    server.await?;

    Ok(())
}
